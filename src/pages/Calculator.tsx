import {
  IonItem,
  IonInput,
  IonLabel,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonText,
  IonList,
  IonSelect,
  IonSelectOption,
  IonPage,
} from "@ionic/react";
import React, { useState } from "react";

const Calculator: React.FC = () => {
  const [current, setCurrent] = useState<string>("0");
  const [isCalculated, setIsCalculated] = useState<boolean>(false);

  const ClearEntry = () => {
    setCurrent("0");
    setIsCalculated(false);
  };

  const Del = () => {
    if (current.length > 0) {
      setCurrent(current.slice(0, -1));
    }
  };

  const SetCurrentByButton = (value: any) => {
    if (
      (isCalculated === true &&
        value.toString() !== "+" &&
        value.toString() !== "-" &&
        value.toString() !== "*" &&
        value.toString() !== "/") ||
      current.toString() === "0"
    ) {
      setCurrent(value);
      setIsCalculated(false);
    } else {
      setIsCalculated(false);
      setCurrent(current.toString() + value.toString());
    }
  };

  const Calculate = () => {
    try {
      const eq = parseFloat(eval(current));
      setIsCalculated(true);
      setCurrent(eq.toString());
    } catch (e) {
      if (e instanceof SyntaxError) {
        alert("Syntax Error!");
      } else {
      }
    }
  };

  return (
    <IonPage>
      <IonGrid>
        <IonRow className="ion-justify-content-center">
          <IonCol>
            <IonItem color="light">
              <IonInput
                style={{ textAlign: "right" }}
                type="text"
                value={current}
                disabled
                onIonChange={(e) => setCurrent(e.detail.value!)}
              ></IonInput>
            </IonItem>
          </IonCol>
        </IonRow>
        <IonRow className="ion-justify-content-center">
          <IonCol className="ion-no-padding ">
            <IonRow>
              <IonCol size="3">
                <IonButton
                  expand="full"
                  onClick={() => ClearEntry()}
                  color="danger"
                >
                  CE
                </IonButton>
              </IonCol>
              <IonCol size="3">
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("(")}
                >
                  (
                </IonButton>
              </IonCol>
              <IonCol size="3">
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton(")")}
                >
                  )
                </IonButton>
              </IonCol>
              <IonCol size="3">
                <IonButton expand="full" onClick={() => Del()} color="warning">
                  Del
                </IonButton>
              </IonCol>
            </IonRow>

            <IonRow>
              <IonCol size="3">
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("7")}
                >
                  7
                </IonButton>
              </IonCol>
              <IonCol size="3">
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("8")}
                >
                  8
                </IonButton>
              </IonCol>
              <IonCol size="3">
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("9")}
                >
                  9
                </IonButton>
              </IonCol>
              <IonCol size="3">
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("/")}
                  color="tertiary"
                >
                  /
                </IonButton>
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol>
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("4")}
                >
                  4
                </IonButton>
              </IonCol>
              <IonCol>
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("5")}
                >
                  5
                </IonButton>
              </IonCol>
              <IonCol>
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("6")}
                >
                  6
                </IonButton>
              </IonCol>
              <IonCol>
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("*")}
                  color="tertiary"
                >
                  *
                </IonButton>
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol>
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("1")}
                >
                  1
                </IonButton>
              </IonCol>
              <IonCol>
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("2")}
                >
                  2
                </IonButton>
              </IonCol>
              <IonCol>
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("3")}
                >
                  3
                </IonButton>
              </IonCol>
              <IonCol>
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("-")}
                  color="tertiary"
                >
                  -
                </IonButton>
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol>
                <IonButton expand="full" onClick={() => SetCurrentByButton(0)}>
                  0
                </IonButton>
              </IonCol>
              <IonCol>
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton(".")}
                >
                  .
                </IonButton>
              </IonCol>
              <IonCol>
                <IonButton
                  expand="full"
                  onClick={() => Calculate()}
                  color="success"
                >
                  =
                </IonButton>
              </IonCol>
              <IonCol>
                <IonButton
                  expand="full"
                  onClick={() => SetCurrentByButton("+")}
                  color="tertiary"
                >
                  +
                </IonButton>
              </IonCol>
            </IonRow>
          </IonCol>
        </IonRow>
      </IonGrid>
    </IonPage>
  );
};

export default Calculator;
