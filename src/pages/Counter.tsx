import {
  IonGrid,
  IonRow,
  IonCol,
  IonLabel,
  IonButton,
  IonPage,
} from "@ionic/react";
import React, { useState } from "react";

const Counter: React.FC = () => {
  const [count, setCount] = useState(0);
  return (
    <IonPage>
      <IonGrid className="ion-text-center ion-margin">
        <IonRow>
          <IonCol>
            <IonLabel>{count}</IonLabel>
          </IonCol>
        </IonRow>
        <IonRow class="ion-justify-content-center">
          <IonCol size-md="2" size-xs="12">
            <IonButton
              onClick={() => setCount((prevCount) => prevCount + 1)}
              expand="full"
            >
              Add Count
            </IonButton>
          </IonCol>
          <IonCol size-md="2" size-xs="12">
            <IonButton
              onClick={() => setCount((prevCount) => prevCount - 1)}
              expand="full"
            >
              Reduce Count
            </IonButton>
          </IonCol>
          <IonCol size-md="2" size-xs="12">
            <IonButton onClick={() => setCount(0)} expand="full">
              Reset Count
            </IonButton>
          </IonCol>
        </IonRow>
      </IonGrid>
    </IonPage>
  );
};

export default Counter;
