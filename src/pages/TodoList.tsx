import {
  IonGrid,
  IonRow,
  IonCol,
  IonCard,
  IonCardContent,
  IonIcon,
  IonPage,
} from "@ionic/react";
import React, { useState } from "react";
import Card from "../components/Card";
import { v4 } from "uuid";
import { todoTypes, todoData } from "../api/TodoList";
import TaskModal from "../modals/TaskModal";
import { createOutline, closeOutline } from "ionicons/icons";

const TodoList: React.FC = () => {
  const initialNewTaskState: todoTypes = {
    id: "",
    title: "",
    content: "",
    color: "primary",
  };
  const [todoList, setTodoList] = useState<todoTypes[]>(todoData);
  const [newTask, setNewTask] = useState<todoTypes>(initialNewTaskState);
  const [modalAction, setModalAction] = useState("add");
  const [showModal, setShowModal] = useState<boolean>(false);

  const divideList = (): any => {
    let rows: any = {};
    let counter = 1;
    const list: todoTypes[] = [...todoList];
    list.push({} as todoTypes);
    list.forEach((todo: todoTypes, idx: number) => {
      rows[counter] = rows[counter] ? [...rows[counter]] : [];
      if (idx % 6 === 0 && idx !== 0) {
        counter++;
        rows[counter] = rows[counter] ? [...rows[counter]] : [];
        rows[counter].push(todo);
      } else {
        rows[counter].push(todo);
      }
    });
    return rows;
  };

  const clearState = () => {
    setModalAction("add");
    setNewTask({ ...initialNewTaskState });
  };

  const AddNewTask = () => {
    const task: todoTypes = {
      id: v4(),
      title: newTask.title,
      content: newTask.content,
      color: newTask.color,
    };
    setTodoList([...todoList, task]);
  };

  const SaveTaskChanges = (id: string) => {
    const updatedList = todoList.map((todo) => {
      if (id === todo.id) {
        return {
          ...todo,
          title: newTask.title,
          content: newTask.content,
          color: newTask.color,
        };
      } else {
        return todo;
      }
    });

    setTodoList(updatedList);
  };

  const DeleteFromList = (id: string) => {
    const updateList = todoList.filter((item) => item.id !== id);
    setTodoList(updateList);
  };

  const SetEditForm = (id: string) => {
    const forEdit = todoList.map((todo) => {
      if (id === todo.id) {
        setNewTask({ ...todo });
        setShowModal(true);
      }
    });
  };

  const itemsComponents = divideList();
  const lastRow: number = Object.keys(itemsComponents).length;
  const lastCol: number = itemsComponents[lastRow].length - 1;

  return (
    <IonPage>
      <IonGrid>
        {Object.keys(itemsComponents).map((row: any) => {
          return (
            <IonRow key={row}>
              {itemsComponents[row].map((item: any, index: number) => {
                return (
                  <>
                    <IonCol size-xs="6" size-md="2" key={index}>
                      {parseInt(row) === lastRow && index === lastCol ? (
                        <IonCard
                          style={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            color: "#fff",
                            cursor: "pointer",
                            width: "50%",
                            height: "auto",
                            backgroundColor: "#00aced",
                          }}
                          onClick={() => setShowModal(true)}
                        >
                          <IonCardContent>
                            <h1>
                              <IonIcon icon={createOutline}></IonIcon>
                            </h1>
                          </IonCardContent>
                        </IonCard>
                      ) : (
                        <Card
                          {...item}
                          key={item.id}
                          DeleteFromList={DeleteFromList}
                          SetEditForm={SetEditForm}
                          setModalAction={setModalAction}
                        />
                      )}
                    </IonCol>
                  </>
                );
              })}
            </IonRow>
          );
        })}
      </IonGrid>

      <TaskModal
        newTask={newTask}
        setNewTask={setNewTask}
        showModal={showModal}
        setShowModal={setShowModal}
        AddNewTask={AddNewTask}
        SaveTaskChanges={SaveTaskChanges}
        clearState={clearState}
        modalAction={modalAction}
      />
    </IonPage>
  );
};

export default TodoList;
