import React, { FC, useState } from "react";
import {
  IonModal,
  IonButton,
  IonItem,
  IonInput,
  IonGrid,
  IonRow,
  IonCol,
  IonTextarea,
  IonLabel,
  IonListHeader,
  IonRadioGroup,
  IonRadio,
  IonList,
  IonAlert,
} from "@ionic/react";
import { todoTypes } from "../api/TodoList";
import { colors } from "../api/Colors";

type Props = {
  modalAction: string;
  showModal: boolean;
  setShowModal: (active: boolean) => void;
  newTask: todoTypes;
  setNewTask: (active: todoTypes) => void;
  SaveTaskChanges: (id: string) => void;
  clearState: () => void;
  AddNewTask: () => void;
};

const TaskModal: FC<Props> = ({
  modalAction,
  showModal,
  setShowModal,
  newTask,
  setNewTask,
  SaveTaskChanges,
  clearState,
  AddNewTask,
}: Props) => {
  const [showAlert, setShowAlert] = useState(false);
  const colorComponent = colors.map((color) => {
    return (
      <IonItem color="dark" key={color}>
        <IonLabel>{color.charAt(0).toUpperCase() + color.slice(1)}</IonLabel>
        <IonRadio slot="start" value={color} color={color} />
      </IonItem>
    );
  });

  return (
    <>
      <IonModal
        isOpen={showModal}
        cssClass="customcss"
        onDidDismiss={() => [clearState(), setShowModal(false)]}
        keyboardClose={false}
      >
        <IonGrid className="ion-no-margin">
          <IonRow>
            <IonCol>
              <IonItem color="dark">
                <IonInput
                  placeholder="Task Title"
                  type="text"
                  value={newTask.title}
                  onIonChange={(e) =>
                    setNewTask({ ...newTask, title: e.detail.value! })
                  }
                ></IonInput>
              </IonItem>
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol>
              <IonItem color="dark">
                <IonTextarea
                  rows={3}
                  placeholder="Enter any notes here..."
                  value={newTask.content}
                  onIonChange={(e) =>
                    setNewTask({ ...newTask, content: e.detail.value! })
                  }
                ></IonTextarea>
              </IonItem>
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol>
              <IonList className="ion-no-padding">
                <IonRadioGroup
                  value={newTask.color}
                  onIonChange={(e) =>
                    setNewTask({ ...newTask, color: e.detail.value! })
                  }
                >
                  <IonListHeader>
                    <IonLabel>Select Color</IonLabel>
                  </IonListHeader>
                  {colorComponent}
                </IonRadioGroup>
              </IonList>
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol>
              <IonButton
                expand="full"
                color="primary"
                onClick={() => [
                  setShowAlert(true),
                  modalAction === "add"
                    ? AddNewTask()
                    : SaveTaskChanges(newTask.id),
                ]}
              >
                {modalAction === "add" ? "Add Task" : "Save Changes"}
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>

        <IonAlert
          isOpen={showAlert}
          backdropDismiss={false}
          onDidDismiss={() => setShowAlert(false)}
          cssClass="alertStyle"
          header={"Success!"}
          message={
            modalAction === "add"
              ? "New Task Successfully Added!"
              : "Changes saved successfully!"
          }
          buttons={[
            {
              text: "Ok",
              handler: () => {
                setShowModal(false);
              },
            },
          ]}
        />

        <IonButton onClick={() => setShowModal(false)}>Close Modal</IonButton>
      </IonModal>
    </>
  );
};
export default TaskModal;
