export type todoTypes = {
  id: string;
  title: string;
  content: string;
  color?: string;
};

export const todoData: todoTypes[] = [
  {
    id: "1",
    title: "Task 1",
    content: "Take out the trash",
    color: "primary",
  },
  {
    id: "2",
    title: "Task 2",
    content: "Grocery shopping",
    color: "tertiary",
  },
  {
    id: "3",
    title: "Task 3",
    content: "Clean gecko tank",
    color: "medium",
  },
  {
    id: "4",
    title: "Task 4",
    content: "Mow lawn",
    color: "light",
  },
  {
    id: "5",
    title: "Task 5",
    content: "Catch up on Development",
    color: "success",
  },
  {
    id: "6",
    title: "New Task",
    content: "Catch up on Development",
    color: "dark",
  },
];
