import {
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonCardContent,
  IonGrid,
  IonRow,
  IonCol,
  IonIcon,
} from "@ionic/react";
import React from "react";
import { trash, pencil, pencilOutline, trashOutline } from "ionicons/icons";

const Card: React.FC = (props: any) => {
  return (
    <>
      <IonCard key={props.id} color={props.color}>
        <div id="card-options">
          <div
            className="class-option"
            onClick={() => [
              props.setModalAction("edit"),
              props.SetEditForm(props.id),
            ]}
          >
            <IonIcon icon={pencilOutline} color="warning"></IonIcon>
          </div>
          <div
            className="class-option"
            onClick={() => props.DeleteFromList(props.id)}
          >
            <IonIcon icon={trashOutline} color="danger"></IonIcon>
          </div>
        </div>
        <IonCardHeader>
          <IonCardTitle>{props.title}</IonCardTitle>
        </IonCardHeader>
        <IonCardContent>{props.content}</IonCardContent>
      </IonCard>
    </>
  );
};

export default Card;
