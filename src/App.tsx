import React from "react";
import {
  IonApp,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonTabs,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonRoute,
  IonIcon,
  IonLabel,
  IonBadge,
} from "@ionic/react";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
import "./App.css";

/* Import Components */
import Counter from "./pages/Counter";
import Calculator from "./pages/Calculator";
import TodoList from "./pages/TodoList";
import { IonReactRouter } from "@ionic/react-router";
import { Redirect, Route } from "react-router-dom";
import {
  calculatorOutline,
  clipboardOutline,
  cashOutline,
} from "ionicons/icons";

const App: React.FC = () => {
  return (
    <IonApp>
      <IonReactRouter>
        <IonHeader>
          <IonToolbar color="primary">
            <IonTitle>Ionic React Exercises</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent color="dark" className="ion-padding ">
          <IonTabs>
            <IonRouterOutlet>
              <Redirect from="/" to="/counter" exact={true} />
              <Route path="/counter" component={Counter} exact={true}></Route>
              <Route
                path="/calculator"
                component={Calculator}
                exact={true}
              ></Route>
              <Route path="/todos" component={TodoList} exact={true}></Route>
            </IonRouterOutlet>
            <IonTabBar slot="top">
              <IonTabButton tab="counter" href="/counter">
                <IonIcon icon={cashOutline} />
                <IonLabel>Counter</IonLabel>
              </IonTabButton>

              <IonTabButton tab="calculator" href="/calculator">
                <IonIcon icon={calculatorOutline} />
                <IonLabel>Calculator</IonLabel>
              </IonTabButton>

              <IonTabButton tab="todos" href="/todos">
                <IonIcon icon={clipboardOutline} />
                <IonLabel>Todo List</IonLabel>
              </IonTabButton>
            </IonTabBar>
          </IonTabs>
        </IonContent>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
